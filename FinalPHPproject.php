
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap PHP $_SERVER </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="jumbotron text-center">
  <h1>CSE383 PHP Final Project</h1>
  <p>Litong Zhang </p> 
</div>

<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">Name</th>
      <th scope="col">Actual Data</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>PHP_SELF</td>
      <td><?php echo $_SERVER['PHP_SELF']; ?></td>
    </tr>
    <tr>
      <td>SERVER_ADDR</td>
      <td><?php echo $_SERVER['SERVER_ADDR']; ?></td>
    </tr>
    <tr>
      <td>REQUEST_TIME</td>
      <td><?php echo $_SERVER['REQUEST_TIME']; ?></td>
    </tr>
    <tr>
      <td>GATEWAY_INTERFACE</td>
      <td><?php echo $_SERVER['GATEWAY_INTERFACE']; ?></td>
    </tr>
    <tr>
      <td>SERVER_SOFTWARE</td>
      <td><?php echo $_SERVER['SERVER_SOFTWARE']; ?></td>
    </tr>
    <tr>
      <td>SERVER_NAME</td>
      <td><?php echo $_SERVER['SERVER_NAME']; ?></td>
    </tr>
    <tr>
      <td>SERVER_PROTOCOL</td>
      <td><?php echo $_SERVER['SERVER_PROTOCOL']; ?></td>
    </tr>
    <tr>
      <td>REQUEST_METHOD</td>
      <td><?php echo $_SERVER['REQUEST_METHOD']; ?></td>
    </tr>
    
    <tr>
      <td>DOCUMENT_ROOT</td>
      <td><?php echo $_SERVER['DOCUMENT_ROOT']; ?></td>
    </tr>
    
    <tr>
      <td>HTTP_ACCEPT_ENCODING</td>
      <td><?php echo $_SERVER['HTTP_ACCEPT_ENCODING']; ?></td>
    </tr>
    <tr>
      <td>HTTP_HOST</td>
      <td><?php echo $_SERVER['HTTP_HOST']; ?></td>
    </tr>
    <tr>
      <td>HTTP_CONNECTION</td>
      <td><?php echo $_SERVER['HTTP_CONNECTION']; ?></td>
    </tr>
    <tr>
      <td>HTTP_ACCEPT_LANGUAGE</td>
      <td><?php echo $_SERVER['HTTP_ACCEPT_LANGUAGE']; ?></td>
    </tr>
    <tr>
      <td>REMOTE_ADDR</td>
      <td><?php echo $_SERVER['REMOTE_ADDR']; ?></td>
    </tr>
    <tr>
      <td>REMOTE_PORT</td>
      <td><?php echo $_SERVER['REMOTE_PORT']; ?></td>
    </tr>

  </tbody>
</table>

</body>
</html>
